

describe("test the whole music-app frontend", () => {
  describe("login page test ", () =>{
    before(()=>{
      //delete all data in the database
      cy.fixture("users").then(users=>{
        let [u1,u2,u3,u4, ...rest] = users
        let four = [u1,u2,u3,u4]
        four.forEach(user =>{
          cy.request('DELETE',`https://blooming-spire-59897.herokuapp.com/user/${user.username}`)
        })
      })
      //add new four user data to the database
      cy.fixture("users").then(users=>{
        let [u1,u2,u3,u4, ...rest] = users
        let four = [u1,u2,u3,u4]
        four.forEach(user =>{
          cy.request('POST','https://blooming-spire-59897.herokuapp.com/user',user)
        })
      })
    })
    it("should have necessary elements on the page",()=>{
      //visit the login page successfully
      cy.visit('http://localhost:8080/#/Login');
      //necessary element on the page
      cy.contains('Login');
      cy.contains("Remember me");
      cy.get('input[placeholder="username"]')
      cy.get('input[placeholder="password"]')

      cy.get('button').contains('Login')
    })
    it("should log in and link to the main page", () =>{

      // do it again
      cy.get('input[placeholder="username"]').type("yyc")
      cy.get('input[placeholder="password"]').type("123456")
      cy.screenshot('login')
      cy.get("button").contains("Login").click()


    })
  })

  describe("music page test",()=> {
   before(()=>{
     // add new four music data to database
     cy.fixture("musics").then(music=>{
       let [u1,u2,u3,u4, ...rest] = music
       let four = [u1,u2,u3,u4]
       four.forEach(music =>{
         cy.request('DELETE',`https://blooming-spire-59897.herokuapp.com/music/${music.name}`)
       })
     })
     cy.fixture("musics").then(music=>{
       let [u1,u2,u3,u4, ...rest] = music
       let four = [u1,u2,u3,u4]
       four.forEach(music =>{
         cy.request('POST','https://blooming-spire-59897.herokuapp.com/music',music)
       })
     })
   })
    it("should have necessary elements on the page",()=>{
     cy.contains("Find Music")
     cy.get('input[placeholder="name"]')
     cy.get('button').contains('Search')
     cy.get('button').contains('Add')
     cy.get('button').contains("Edit")
     cy.get('button').contains("Delete")
   })

    it("should add music information to the table",()=>{
      cy.get('button').contains('Add').click()
      cy.screenshot('add music')
      cy.get('input[name="name"]').type("我是谁")
      cy.get('input[name="singer"]').type("群星")
      cy.get('input[name="album"]').type("unknown")
      cy.get('textarea[name="intro"]').type("2019 top 10")
      cy.get('button').contains('Submit').click()
      cy.screenshot('add music finish')
      cy.get('table').contains("我是谁")
      cy.get('table').contains("群星")
      cy.get('table').contains("unknown")
      cy.get('table').contains("2019 top 10")
    })

    it("can search music information",()=>{
      cy.get('input[placeholder="name"]').type("我是谁")
      cy.get('button').contains('Search').click()
      cy.screenshot('search music')
      cy.get('table').contains("我是谁")
      cy.get('table').contains("群星")
      cy.get('table').contains("unknown")
      cy.get('table').contains("2019 top 10")
      cy.get('input[name="searchtext"]')
        .type("{selectall}",{ parseSpecialCharSequences: true })
        .type("{backspace}",{ parseSpecialCharSequences: true })

    })

    it("can edit the music information",()=>{
      cy.get('tbody')
        .find("tr")
        .eq(0)
        .find("td")
        .eq(5)
        .get('button').contains('Edit').click()

      cy.get('input[name="name"]')
        .type("{selectall}",{ parseSpecialCharSequences: true })
        .type("{backspace}",{ parseSpecialCharSequences: true })
        .type("who i am")

      cy.get('input[name="singer"]')
        .type("{selectall}",{ parseSpecialCharSequences: true })
        .type("{backspace}",{ parseSpecialCharSequences: true })
        .type("all stars")

      cy.get('input[name="album"]')
        .type("{selectall}",{ parseSpecialCharSequences: true })
        .type("{backspace}",{ parseSpecialCharSequences: true })
        .type("unknown")

      cy.get('textarea[name="intro"]')
        .type("{selectall}",{ parseSpecialCharSequences: true })
        .type("{backspace}",{ parseSpecialCharSequences: true })
        .type("2020 top 10")
      cy.screenshot('edit music')
      cy.get('button').contains('Submit').click()
      cy.get('button').contains('Cancel').click()
    })
    it("can delete the music information",()=>{
      cy.get('tbody')
        .find("tr")
        .eq(0)
        .find("td")
        .eq(5)
        .get('button').contains('Delete').click()
      cy.screenshot('delete music')
        .get('button').contains('Sure').click()

    })

  })
describe("leadboard page test",()=>{
  before(()=>{
    //
    cy.fixture("leadboard").then(leadboard=>{
      let [u1,u2,u3,u4, ...rest] = leadboard
      let four = [u1,u2,u3,u4]
      four.forEach(leadboard =>{
        cy.request('DELETE',`https://blooming-spire-59897.herokuapp.com/leadboard/${leadboard.name}`)
      })
    })
    cy.fixture("leadboard").then(users=>{
      let [u1,u2,u3,u4, ...rest] = users
      let four = [u1,u2,u3,u4]
      four.forEach(leadboard =>{
        cy.request('POST','https://blooming-spire-59897.herokuapp.com/leadboard',leadboard)
      })
    })
  })
  it("can change to the leadboard page",()=>{
    cy.get('[class="el-submenu"]').contains('Leadboard').click()
    cy.get('[class="el-menu-item"]').contains('Leadboard').click()
    cy.contains('Leadboard')
    cy.get('button').contains('Search')
    cy.get('button').contains('Add')
    cy.get('button').contains("Edit")
    cy.get('button').contains("Delete")
  })

  it("should add leadboard information to the table",()=>{
    cy.get('button').contains('Add').click()
    cy.get('input[name="name"]').type("流行音乐排行榜")
    cy.screenshot('add leadboard')
    cy.get('button').contains('Submit').click()
    cy.get('table').contains("流行音乐排行榜")

  })

  it("can search leadboard information",()=>{
    cy.get('input[placeholder="name"]').type("流行音乐排行榜")
    cy.get('button').contains('Search').click()
    cy.screenshot('search music')
    cy.get('table').contains("流行音乐排行榜")
    cy.get('input[name="searchtext"]')
      .type("{selectall}",{ parseSpecialCharSequences: true })
      .type("{backspace}",{ parseSpecialCharSequences: true })

  })

  it("can edit the music information",()=>{
    cy.get('tbody')
      .find("tr")
      .eq(0)
      .find("td")
      .eq(3)
      .get('button').contains('Edit').click()
    cy.screenshot('edit music')

    cy.get('input[name="name"]')
      .type("{selectall}",{ parseSpecialCharSequences: true })
      .type("{backspace}",{ parseSpecialCharSequences: true })
      .type("chinese pop music top10")


    cy.get('button').contains('Submit').click()
    cy.get('button').contains('Cancel').click()

  })
  it("can delete the music information",()=>{
    cy.get('tbody')
      .find("tr")
      .eq(0)
      .find("td")
      .eq(3)
      .get('button').contains('Delete').click()
    cy.screenshot('delete music')
      .get('button').contains('Sure').click()

  })
})

  describe("User page test",()=>{

    it("can change to the User page",()=>{
      cy.get('[class="el-submenu"]').contains('User').click()
      cy.get('[class="el-menu-item"]').contains('Change Password').click()
      cy.contains('Change Password')
      cy.get('button').contains('Search')
      cy.get('button').contains('Add')
      cy.get('button').contains("Edit")
      cy.get('button').contains("Delete")
    })

    it("should change the password",()=>{
      cy.get('input[placeholder="password"]').type("717235460")
      cy.screenshot('change password')
      cy.get('button').contains('Submit').click()


    })


    it("can log out",()=>{
      cy.get('[class=el-dropdown]').click()
    })
  })
})
